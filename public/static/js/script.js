/**
 * Created by Administrator on 10/19/15.
 */
jQuery(document).ready(function(){
    var recipes = [
        {
            "name": "grilled cheese on toast",
            "ingredients": [
                { "item":"bread", "amount":"2", "unit":"slices"},
                { "item":"cheese", "amount":"2", "unit":"slices"}
            ]
        }
        ,
        {
            "name": "salad sandwich",
            "ingredients": [
                { "item":"bread", "amount":"2", "unit":"slices"},
                { "item":"mixed salad", "amount":"100", "unit":"grams"}
            ]
        }
    ];
    var file;

    jQuery('#fileupload').fileupload({
        url: 'upload',
        dataType: 'json',
        done: function (e, data) {
            file = data.result;

            $('#progress').addClass('hidden');
            $('.filename').html(data.files[0].name);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress').removeClass('hidden');
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    jQuery("button.analysis").on("click", function(){
        if(file){
            jQuery.ajax({
                url: "determine",
                type: "POST",
                data: {recipes: recipes, file: file}
            })
                .success(function(response){
                    if ( JSON.parse(response).type == 0 )
                    {
                        jQuery('.result p').html('<span>' + JSON.parse(response).result + '</span>');
                    }
                    else
                    {
                        jQuery('.result p').html('<label>Last order is </label><span>' + JSON.parse(response).result + '</span>');
                    }
                })
                .error(function (response){
                    console.log(response);
                    jQuery('.error').html('<label>Can\'t read your file. Please choose it agian or check your permission.</label>')
                });
        }
        else
        {
            jQuery('.error').html('<label>Can\'t read your file. Please choose it agian or check your permission.</label>');
        }
    });
});