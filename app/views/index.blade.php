<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Test - CSV parsing</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    {{ HTML::style('static/css/jquery.fileupload.css') }}
    {{ HTML::style('static/css/style.css') }}

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    {{ HTML::script('static/js/jquery.ui.widget.js')}}
    {{ HTML::script('static/js/jquery.fileupload.js')}}
    {{ HTML::script('static/js/script.js')}}
<body>
    <div class="container">
        <div class="row">
            <div class="recipes">
                <label>Recipes</label>
                <ul>
                    <li>Grilled cheese on toast</li>
                    <li>Salad sandwich</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <span class="btn btn-success fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Select file</span>
                <!-- The file input field used as target for the file upload widget -->
                <input id="fileupload" type="file" name="file" multiple>
            </span>
            <div id="progress" class="progress hidden">
                <div class="progress-bar progress-bar-success"></div>
            </div>
            <div class="filename"></div>
            <div class="error"></div>
        </div>
        <div class="row">
            <div class="analytics">
                <button name="analysis" class="analysis btn btn-primary">Analysis</button>
                <div class="result">
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
