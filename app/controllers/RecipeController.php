<?php

use Illuminate\Support\Facades\Request;

class RecipeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function determine()
	{
        $recipeList = Input::get('recipes');
        $fileInfo = Input::get('file');
        $csvPath = $fileInfo['filepath'] . $fileInfo['filename'];

        if(!file_exists($csvPath) || !is_readable($csvPath))
            return json_encode( array('result' => "Can't read your file. Please choose it agian or check your permission.", 'type' => "error") );

        $data = array();
        if (($handle = fopen($csvPath, 'r')) !== FALSE)
        {
            while (($row = fgetcsv($handle, 100, ',')) !== FALSE)
            {
                $data[] = $row;
            }
            fclose($handle);
        }

        $ingredients = array();
        foreach ( $data as $element )
        {
            $ingredients[$element[0]] = $element;
        }

        $available = array();;
        foreach ( $recipeList as $recipe )
        {
            $count = 0;
            $array = array();
            foreach( $recipe['ingredients'] as $ingred )
            {
                if ( array_key_exists($ingred['item'], $ingredients) )
                {
                    $count ++;
                    $array[$ingred['item']] = $ingred;
                    $date = $ingredients[$ingred['item']][3];
                    $array[$ingred['item']]['date_by'] = strtotime(str_replace('/', '-', $date));

                    if ( $count == count($recipe['ingredients']) )
                    {
                        $available[$recipe['name']] = $array;
                    }
                }
                else
                {
                    break;
                }
            }
        }

        if ( count($available) == 0 )
        {
            $response = "Order Takeout";
        }

        if ( count($available) == 1 )
        {
            $response = $available[0];
        }

        if ( count($available) > 1 )
        {
            $requires = array();
            foreach ( $available as $key => $element )
            {
                if ( empty($requires) )
                {
                    $requires = $element;
                }
                else
                {
                    $requires = array_merge($requires, $element);
                }
            }

            uasort($requires, function($a, $b) {
                $ad = $a['date_by'];
                $bd = $b['date_by'];

                if ($ad == $bd) {
                    return 0;
                }

                return $ad < $bd ? 1 : -1;
            });

            $val = array();
            foreach ( $requires as $key => $values )
            {
                $val[$values['date_by']][] = $key;
            }
            $selector = array();
            foreach ( $available as $key => $element )
            {
                $sum = 0;
                foreach ( $element as $ingred )
                {
                    $order = 0;
                    foreach ( $val as $v )
                    {
                        if ( in_array($ingred['item'], $v) )
                        {
                            break;
                        }

                        $order ++;
                    }
                    $sum += $ingred['date_by'] * (count($element) - $order);
                }

                $avg = $sum / (count($element) * 100);
                $selector[$avg] = $key;
            }

            krsort($selector);

            $response = current($selector);
        }

        return json_encode( array('result' => $response, 'type' => count($available)) );
	}

    public function fileUpload()
    {
        $file = Input::file('file');

        if($file) {

            $destinationPath = public_path() . '/uploads/';
            $filename = Str::random(20) . '-' . $file->getClientOriginalName();

            $upload_success = Input::file('file')->move($destinationPath, $filename);

            if ($upload_success) {
                return json_encode(array('filename' => $filename, 'filepath' => $destinationPath, 'status' => "success"));
            } else {
                return json_encode(array('status' => 'error'));
            }
        }
    }

}
